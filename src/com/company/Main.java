package com.company;

import java.math.BigInteger;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();
        String express = "^(\\s*)(\\+|-?)(\\d+)(\\s+)(\\+|-?)(\\d+)(\\s*)$";//正则表达式匹配$a+$b，名为express
        //匹配开始；匹配'+'(要转义)或'-'(不用转义){0,1}次；匹配数字{1,}次；匹配空格{1，}次；匹配符号；匹配数字；结束
        Pattern pattern = Pattern.compile(express);//将express编译成pattern
        Matcher match = pattern.matcher(input);//match为匹配结果
/**
 * 以上为输入匹配处理
 */
        if (match.find()) {//如果找到结果
            char symble_a = match.group(2).length() == 0 ? '+' : match.group(2).charAt(0);//当a前面没有+-符号时，group(1)为空（如果不讨论是否为空的话，charAt报错）
            String a = match.group(3);
            char symble_b = match.group(5).length() == 0 ? '+' : match.group(5).charAt(0);//charAt是吧String型的match.group转换成char型
            String b = match.group(6);

            BigInteger num_a = new BigInteger(a);
            BigInteger num_b = new BigInteger(b);
            BigInteger zero = new BigInteger("0");

            if (symble_a == '-') {
                num_a = zero.subtract(num_a);
            }
            if (symble_b == '-') {
                num_b = zero.subtract(num_b);
            }
            System.out.print(num_a.add(num_b));
        } else {
            System.out.print("WRONG FORMAT!");
        }
    }
}
